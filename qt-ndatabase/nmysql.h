#ifndef NMYSQL_H
#define NMYSQL_H

#include <QObject>
#include <QDebug>

#include "ndatasession.h"

// mysql 数据库连接
class NMysql :public NLimitSession
{
public:
    explicit NMysql(QString host,int port,QString user,QString password,QString dbName):NLimitSession(){
        auto connectionName = QSqlDatabase::defaultConnection;
        if(!QSqlDatabase::contains(connectionName)){
            this->connection = QSqlDatabase::addDatabase("QMYSQL",connectionName);
        }else{
            this->connection = QSqlDatabase::database(connectionName);
        }
        this->connection.setHostName(host);
        this->connection.setPort(port);

        this->connection.setUserName(user);
        this->connection.setPassword(password);

        this->connection.setDatabaseName(dbName);
        this->connection.setConnectOptions("MYSQL_OPT_RECONNECT=1");
    }

    ///
    /// \brief QMysql
    /// \param db QVariantMap{host,port,user,pwd,db}
    ///
    explicit NMysql(QVariantMap db):NLimitSession(){
        if(db.isEmpty()){
            return;
        }
        QString host = db.contains("host") ? db["host"].toString() : QString("127.0.0.1");
        int port = db.contains("port") ? db["port"].toInt() : 3306;
        QString user = db.contains("user") ? db["user"].toString() : QString("root");
        if(user.isEmpty()){
            user = db.contains("userName") ? db["userName"].toString() : QString();
        }
        QString password = db.contains("pwd") ? db["pwd"].toString() : QString();
        if(password.isEmpty()){
            password = db.contains("password") ? db["password"].toString() : QString();
        }
        QString name = db.contains("db") ? db["db"].toString() : QString();
        if(name.isEmpty()){
            name = db.contains("dbName") ? db["dbName"].toString() : QString();
        }
        if(name.isEmpty()){
            name = db.contains("database") ? db["database"].toString() : QString();
        }

        auto connectionName = QSqlDatabase::defaultConnection;
        if(!QSqlDatabase::contains(connectionName)){
            this->connection = QSqlDatabase::addDatabase("QMYSQL",connectionName);
        }else{
            this->connection = QSqlDatabase::database(connectionName);
        }

        this->connection.setHostName(host);
        this->connection.setPort(port);

        this->connection.setUserName(user);
        this->connection.setPassword(password);

        this->connection.setDatabaseName(name);
        this->connection.setConnectOptions("MYSQL_OPT_RECONNECT=1");
    }
};

#endif // NMYSQL_H
