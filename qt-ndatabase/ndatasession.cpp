#include "ndatasession.h"

NDataSession::NDataSession(){}

NDataSession::NDataSession(QSqlDatabase connection){
    this->connection = connection;
}

NDataSession::~NDataSession(){
    this->close();
}

QString NDataSession::options(){
    return this->connection.connectOptions();
}

void NDataSession::options(QString opt){
    this->connection.setConnectOptions(opt);
}

bool NDataSession::open(){
    if(!this->connection.isValid()){
        return false;
    }
    if(this->connection.isOpen()){
        return true;
    }
    if(!this->connection.open()){
        if(this->connection.isOpenError()){
            this->print(this->connection.lastError());
        }
        return false;
    }
    return true;
}

bool NDataSession::close(bool commit){
    if(!this->connection.isValid()){
        return true;
    }
    if(!this->connection.isOpen()){
        return true;
    }
    if(commit){
        this->connection.commit();
    }
    this->connection.close();
    return true;
}

bool NDataSession::isOpen(){
    if(!this->connection.isValid()){
        return false;
    }
    return this->connection.isOpen();
}

bool NDataSession::isValid(){
    return this->connection.isValid();
}

bool NDataSession::transaction(){
    if(!this->isOpen()){
        return false;
    }
    return this->connection.transaction();
}

bool NDataSession::rollback(){
    if(!this->isOpen()){
        return false;
    }
    return this->connection.rollback();
}

bool NDataSession::commit(){
    if(!this->isOpen()){
        return false;
    }
    return this->connection.commit();
}

void NDataSession::print(QSqlQuery query){
    qDebug() << "sql" << query.lastQuery();
    auto values = query.boundValues();
    auto i = values.constBegin();
    while (i != values.constEnd()) {
        qDebug() << i.value();
        ++i;
    }
}

void NDataSession::print(QSqlError error){
    qWarning() << "error code" << error.nativeErrorCode() << "message" << error.text();
}

QStringList NDataSession::showTable(){
    if(!this->connection.isValid()){
        return QStringList();
    }
    return this->connection.tables();
}

bool NDataSession::hasTable(QString name){
    auto table = this->showTable();
    return table.indexOf(name) != -1;
}

NDataSet NDataSession::list(QString sql,QVariantList args){
    if(sql.isEmpty()){
        auto message = QString("sql is empty");
        auto error = QSqlError(message,message,QSqlError::StatementError);
        return NDataSet(sql,args,error);
    }
    if(!this->isOpen()){
        this->open();
    }
    if(!this->open()){
        auto message = QString("can not open database connection");
        auto error = QSqlError(message,message,QSqlError::ConnectionError);
        return NDataSet(sql,args,error);
    }
    auto create = this->create(sql);
    if(!create.first){
        auto message = QString("prepare sql error");
        auto error = QSqlError(message,message,QSqlError::StatementError);
        return NDataSet(sql,args,error);
    }
    auto query = create.second;
    this->bind(query,args);
    if(!query.exec()){
        return NDataSet(sql,args,query.lastError());
    }
    return NDataSet(query);
}

NDataTable NDataSession::table(QString sql,QVariantList args){
    return this->list(sql,args).first();
}

bool NDataSession::excute(QString sql,QVariantList args){
    if(sql.isEmpty()){
        qWarning() << "sql is empty";
        return false;
    }
    if(!this->isOpen()){
        this->open();
    }
    if(!this->open()){
        qWarning() << "can not open database connection";
        return false;
    }
    qDebug() << "sql" << sql;
    auto create = this->create(sql);
    if(!create.first){
        return false;
    }
    auto query = create.second;
    this->bind(query,args);
    if(!query.exec()){
        this->print(query.lastError());
        return false;
    }
    return true;
}

NDataTable NDataSession::table(QString tableName,QString where,QString orderBy){
    if(orderBy.isEmpty()){
        return this->table(tableName,where,QStringList());
    }
    return this->table(tableName,where,QStringList{orderBy});
}

NDataTable NDataSession::table(QString tableName,QString where,QStringList orderBy){
    if(tableName.isEmpty()){
        return NDataTable();
    }
    QString sql = QString("select * from %1").arg(tableName);
    if(!where.isEmpty()){
        sql = QString("%1 where %2").arg(sql).arg(where);
    }
    if(!orderBy.isEmpty()){
        sql = QString("%1 order by %2").arg(sql).arg(orderBy.join(","));
    }
    sql.append(";");
    return this->list(sql).first();
}

NDataTable NDataSession::table(QString tableName,QVariantMap where,QString orderBy){
    if(orderBy.isEmpty()){
        return this->table(tableName,where,QStringList());
    }
    return this->table(tableName,where,QStringList{orderBy});
}

NDataTable NDataSession::table(QString tableName,QVariantMap where,QStringList orderBy){
    if(tableName.isEmpty()){
        return NDataTable();
    }
    QString sql = QString("select * from %1").arg(tableName);

    QStringList bind = QStringList();
    QVariantList args = QVariantList();

    QStringList filter = where.keys();
    for(int i=0;i<filter.count();i++){
        auto column = filter.at(i);
        bind.append(QString("%1=?").arg(column));
        args.append(where.value(column));
    }
    if(!where.isEmpty()){
        sql = QString("%1 where %2").arg(sql).arg(bind.join(" and "));
    }
    if(!orderBy.isEmpty()){
        sql = QString("%1 order by %2").arg(sql).arg(orderBy.join(","));
    }
    sql.append(";");
    return this->list(sql,args).first();
}

NDataTable NDataSession::table(QString tableName,QStringList columns,QString where,QString orderBy){
    if(orderBy.isEmpty()){
        return this->table(tableName,columns,where,QStringList());
    }
    return this->table(tableName,columns,where,QStringList{orderBy});
}

NDataTable NDataSession::table(QString tableName,QStringList columns,QString where,QStringList orderBy){
    if(tableName.isEmpty()){
        return NDataTable();
    }
    if(columns.isEmpty()){
        return NDataTable();
    }
    QString sql = QString("select %1 from %2").arg(columns.join(",")).arg(tableName);
    if(!where.isEmpty()){
        sql = QString("%1 where %2").arg(sql).arg(where);
    }
    if(!orderBy.isEmpty()){
        sql = QString("%1 order by %2").arg(sql).arg(orderBy.join(","));
    }
    sql.append(";");
    return this->list(sql).first();
}

NDataTable NDataSession::table(QString tableName,QStringList columns,QVariantMap where,QString orderBy){
    if(orderBy.isEmpty()){
        return this->table(tableName,columns,where,QStringList());
    }
    return this->table(tableName,columns,where,QStringList{orderBy});
}

NDataTable NDataSession::table(QString tableName,QStringList columns,QVariantMap where,QStringList orderBy){
    if(tableName.isEmpty()){
        return NDataTable();
    }
    if(columns.isEmpty()){
        return NDataTable();
    }
    QString sql = QString("select %1 from %2").arg(columns.join(",")).arg(tableName);

    QStringList bind = QStringList();
    QVariantList args = QVariantList();

    QStringList filter = where.keys();
    for(int i=0;i<filter.count();i++){
        auto column = filter.at(i);
        bind.append(QString("%1=?").arg(column));
        args.append(where.value(column));
    }
    if(!where.isEmpty()){
        sql = QString("%1 where %2").arg(sql).arg(bind.join(" and "));
    }
    if(!orderBy.isEmpty()){
        sql = QString("%1 order by %2").arg(sql).arg(orderBy.join(","));
    }
    sql.append(";");
    return this->list(sql,args).first();
}

bool NDataSession::add(QString tableName,QVariantMap data){
    if(tableName.isEmpty()){
        return false;
    }
    if(data.isEmpty()){
        return false;
    }
    QString sql = QString("insert into %1(%2) values(%3);");

    QStringList bind = QStringList();
    QVariantList args = QVariantList();

    QStringList columns = data.keys();
    for(int i=0;i<columns.count();i++){
        auto column = columns.at(i);
        bind.append(QString(":%1").arg(column));
        args.append(data.value(column));
    }
    sql = sql.arg(tableName).arg(columns.join(",")).arg(bind.join(","));
    return this->excute(sql,args);
}

bool NDataSession::add(QString tableName,QStringList columns,QVariantMap data){
    if(tableName.isEmpty()){
        return false;
    }
    if(columns.isEmpty()){
        return false;
    }
    if(data.isEmpty()){
        return false;
    }
    QString sql = QString("insert into %1(%2) values(%3);");

    QStringList bind = QStringList();
    QVariantList args = QVariantList();

    for(int i=0;i<columns.count();i++){
        auto column = columns.at(i);
        bind.append(QString(":%1").arg(column));
        args.append(data.contains(column)?data.value(column):QVariant());
    }
    sql = sql.arg(tableName).arg(columns.join(",")).arg(bind.join(","));
    return this->excute(sql,args);
}

bool NDataSession::add(QString tableName,QList<QVariantMap> data){
    if(tableName.isEmpty()){
        return false;
    }
    if(data.isEmpty()){
        return false;
    }

    // 是否出错
    auto error = false;
    for(int i=0;i<data.count();i++){
        auto item = data.at(i);
        if(!this->add(tableName,item)){
            error = true;
            break;
        }
    }
    return !error;
}

bool NDataSession::add(QString tableName,QStringList columns,QList<QVariantMap> data){
    if(tableName.isEmpty()){
        return false;
    }
    if(columns.isEmpty()){
        return false;
    }
    if(data.isEmpty()){
        return false;
    }
    QString sql = QString("insert into %1(%2) values %3;");

    QStringList bind = QStringList();
    QVariantList args = QVariantList();

    for(int i=0;i<data.count();i++){
        auto item = data.at(i);
        QStringList temp = QStringList();
        for(int j=0;j<columns.count();j++){
            auto column = columns.at(j);
            temp.append(QString(":%1_%2").arg(column).arg(i));
            args.append(item.contains(column)?item.value(column):QVariant());
        }
        bind.append(QString("(%1)").arg(temp.join(",")));
    }
    sql = sql.arg(tableName).arg(columns.join(",")).arg(bind.join(","));
    return this->excute(sql,args);
}

bool NDataSession::remove(QString tableName,QString where){
    if(tableName.isEmpty()){
        return false;
    }
    QString sql = QString("delete from %1").arg(tableName);
    if(!where.isEmpty()){
        sql = QString("%1 where %2").arg(sql).arg(where);
    }
    sql.append(";");
    return this->excute(sql);
}

bool NDataSession::remove(QString tableName,QVariantMap where){
    if(tableName.isEmpty()){
        return false;
    }
    QString sql = QString("delete from %1").arg(tableName);

    QStringList bind = QStringList();
    QVariantList args = QVariantList();

    QStringList filter = where.keys();
    for(int i=0;i<filter.count();i++){
        auto column = filter.at(i);
        bind.append(QString("%1=?").arg(column));
        args.append(where.value(column));
    }
    if(!where.isEmpty()){
        sql = QString("%1 where %2").arg(sql).arg(bind.join(" and "));
    }
    sql.append(";");
    return this->excute(sql,args);
}

bool NDataSession::update(QString tableName,QVariantMap data,QString where){
    if(tableName.isEmpty()){
        return false;
    }
    if(data.isEmpty()){
        return false;
    }
    QString sql = QString("update %1 set %2");

    QVariantList args = QVariantList();
    QStringList bind = QStringList();

    QStringList columns = data.keys();
    for(int i=0;i<columns.count();i++){
        auto column = columns.at(i);
        bind.append(QString("%1=?").arg(column));
        args.append(data.value(column));
    }
    sql = sql.arg(tableName).arg(bind.join(","));
    if(!where.isEmpty()){
        sql = QString("%1 where %2").arg(sql).arg(where);
    }
    sql.append(";");
    return this->excute(sql,args);
}

bool NDataSession::update(QString tableName,QVariantMap data,QVariantMap where){
    if(tableName.isEmpty()){
        return false;
    }
    if(data.isEmpty()){
        return false;
    }
    QString sql = QString("update %1 set %2");

    QVariantList args = QVariantList();
    QStringList bind = QStringList();

    QStringList columns = data.keys();
    for(int i=0;i<columns.count();i++){
        auto column = columns.at(i);
        bind.append(QString("%1=?").arg(column));
        args.append(data.value(column));
    }
    sql = sql.arg(tableName).arg(bind.join(","));

    bind.clear();
    QStringList filter = where.keys();
    for(int i=0;i<filter.count();i++){
        auto column = filter.at(i);
        bind.append(QString("%1=?").arg(column));
        args.append(where.value(column));
    }
    if(!where.isEmpty()){
        sql = QString("%1 where %2").arg(sql).arg(bind.join(" and "));
    }
    sql.append(";");
    return this->excute(sql,args);
}

bool NDataSession::update(QString tableName,QStringList columns,QVariantMap data,QString where){
    if(tableName.isEmpty()){
        return false;
    }
    if(columns.isEmpty()){
        return false;
    }
    if(data.isEmpty()){
        return false;
    }
    QString sql = QString("update %1 set %2");

    QVariantList args = QVariantList();
    QStringList bind = QStringList();

    for(int i=0;i<columns.count();i++){
        auto column = columns.at(i);
        bind.append(QString("%1=?").arg(column));
        args.append(data.contains(column)?data.value(column):QVariant());
    }
    sql = sql.arg(tableName).arg(bind.join(","));
    if(!where.isEmpty()){
        sql = QString("%1 where %2").arg(sql).arg(where);
    }
    sql.append(";");
    return this->excute(sql,args);
}

bool NDataSession::update(QString tableName,QStringList columns,QVariantMap data,QVariantMap where){
    if(tableName.isEmpty()){
        return false;
    }
    if(data.isEmpty()){
        return false;
    }
    QString sql = QString("update %1 set %2");

    QVariantList args = QVariantList();
    QStringList bind = QStringList();

    for(int i=0;i<columns.count();i++){
        auto column = columns.at(i);
        bind.append(QString("%1=?").arg(column));
        args.append(data.contains(column)?data.value(column):QVariant());
    }
    sql = sql.arg(tableName).arg(bind.join(","));

    bind.clear();
    QStringList filter = where.keys();
    for(int i=0;i<filter.count();i++){
        auto column = filter.at(i);
        bind.append(QString("%1=?").arg(column));
        args.append(where.value(column));
    }
    if(!where.isEmpty()){
        sql = QString("%1 where %2").arg(sql).arg(bind.join(" and "));
    }
    sql.append(";");
    return this->excute(sql,args);
}

int NDataSession::count(QString tableName,QString where){
    if(tableName.isEmpty()){
        return 0;
    }
    QString sql = QString("select count(1) as count from %1").arg(tableName);
    if(!where.isEmpty()){
        sql = QString("%1 where %2").arg(sql).arg(where);
    }
    sql.append(";");
    auto data = this->list(sql).first();
    auto scalar = data.scalar();
    if(!scalar.isValid()){
        return 0;
    }
    return scalar.toInt();
}

int NDataSession::count(QString tableName,QVariantMap where){
    if(tableName.isEmpty()){
        return 0;
    }
    QString sql = QString("select count(1) as count from %1").arg(tableName);
    QStringList bind = QStringList();
    QVariantList args = QVariantList();
    QStringList filter = where.keys();
    for(int i=0;i<filter.count();i++){
        auto column = filter.at(i);
        bind.append(QString("%1=?").arg(column));
        args.append(where.value(column));
    }
    if(!where.isEmpty()){
        sql = QString("%1 where %2").arg(sql).arg(bind.join(" and "));
    }
    sql.append(";");
    auto data = this->list(sql,args).first();
    auto scalar = data.scalar();
    if(!scalar.isValid()){
        return 0;
    }
    return scalar.toInt();
}

bool NDataSession::exist(QString tableName,QString where){
    return this->count(tableName,where) > 0;
}

bool NDataSession::exist(QString tableName,QVariantMap where){
    return this->count(tableName,where) > 0;
}

QPair<bool,QSqlQuery> NDataSession::create(QString sql){
    QPair<bool,QSqlQuery> pair;
    // 存储过程
    if(sql.startsWith("call")){
        pair.second = QSqlQuery(sql,this->connection);
        pair.first = true;
    }else{
        pair.second = QSqlQuery(this->connection);
        pair.first = pair.second.prepare(sql);
        if(!pair.first){
            qWarning() << sql << pair.second.lastError().text();
        }
    }
    return pair;
}

void NDataSession::bind(QSqlQuery query,QVariantList args){
    if(args.count() == 0){
        return;
    }
    for(int i=0;i<args.count();i++){
        query.bindValue(i,args.at(i));
    }
}

NLimitSession::NLimitSession():NDataSession(){}

NLimitSession::NLimitSession(QSqlDatabase db):NDataSession(db){}

NDataTable NLimitSession::limit(QString tableName,QString where,QString orderBy,QString limit){
    if(orderBy.isEmpty()){
        return this->limit(tableName,where,QStringList(),limit);
    }
    return this->limit(tableName,where,QStringList{orderBy},limit);
}

NDataTable NLimitSession::limit(QString tableName,QString where,QStringList orderBy,QString limit){
    if(tableName.isEmpty()){
        return NDataTable();
    }
    QString sql = QString("select * from %1").arg(tableName);
    if(!where.isEmpty()){
        sql = QString("%1 where %2").arg(sql).arg(where);
    }
    if(!orderBy.isEmpty()){
        sql = QString("%1 order by %2").arg(sql).arg(orderBy.join(","));
    }
    if(!limit.isEmpty()){
        sql = QString("%1 limit %2").arg(sql).arg(limit);
    }
    sql.append(";");
    return this->list(sql).first();
}

NDataTable NLimitSession::limit(QString tableName,QVariantMap where,QString orderBy,QString limit){
    if(orderBy.isEmpty()){
        return this->limit(tableName,where,QStringList(),limit);
    }
    return this->limit(tableName,where,QStringList{orderBy},limit);
}

NDataTable NLimitSession::limit(QString tableName,QVariantMap where,QStringList orderBy,QString limit){
    if(tableName.isEmpty()){
        return NDataTable();
    }
    QString sql = QString("select * from %1").arg(tableName);

    QStringList bind = QStringList();
    QVariantList args = QVariantList();

    QStringList filter = where.keys();
    for(int i=0;i<filter.count();i++){
        auto column = filter.at(i);
        bind.append(QString("%1=?").arg(column));
        args.append(where.value(column));
    }
    if(!where.isEmpty()){
        sql = QString("%1 where %2").arg(sql).arg(bind.join(" and "));
    }
    if(!orderBy.isEmpty()){
        sql = QString("%1 order by %2").arg(sql).arg(orderBy.join(","));
    }
    if(!limit.isEmpty()){
        sql = QString("%1 limit %2").arg(sql).arg(limit);
    }
    sql.append(";");
    return this->list(sql,args).first();
}

NDataTable NLimitSession::limit(QString tableName,QStringList columns,QString where,QString orderBy,QString limit){
    if(orderBy.isEmpty()){
        return this->limit(tableName,columns,where,QStringList(),limit);
    }
    return this->limit(tableName,columns,where,QStringList{orderBy},limit);
}

NDataTable NLimitSession::limit(QString tableName,QStringList columns,QString where,QStringList orderBy,QString limit){
    if(tableName.isEmpty()){
        return NDataTable();
    }
    if(columns.isEmpty()){
        return NDataTable();
    }
    QString sql = QString("select %1 from %2").arg(columns.join(",")).arg(tableName);
    if(!where.isEmpty()){
        sql = QString("%1 where %2").arg(sql).arg(where);
    }
    if(!orderBy.isEmpty()){
        sql = QString("%1 order by %2").arg(sql).arg(orderBy.join(","));
    }
    if(!limit.isEmpty()){
        sql = QString("%1 limit %2").arg(sql).arg(limit);
    }
    sql.append(";");
    return this->list(sql).first();
}

NDataTable NLimitSession::limit(QString tableName,QStringList columns,QVariantMap where,QString orderBy,QString limit){
    if(orderBy.isEmpty()){
        return this->limit(tableName,columns,where,QStringList(),limit);
    }
    return this->limit(tableName,columns,where,QStringList{orderBy},limit);
}

NDataTable NLimitSession::limit(QString tableName,QStringList columns,QVariantMap where,QStringList orderBy,QString limit){
    if(tableName.isEmpty()){
        return NDataTable();
    }
    if(columns.isEmpty()){
        return NDataTable();
    }
    QString sql = QString("select %1 from %2").arg(columns.join(",")).arg(tableName);

    QStringList bind = QStringList();
    QVariantList args = QVariantList();

    QStringList filter = where.keys();
    for(int i=0;i<filter.count();i++){
        auto column = filter.at(i);
        bind.append(QString("%1=?").arg(column));
        args.append(where.value(column));
    }
    if(!where.isEmpty()){
        sql = QString("%1 where %2").arg(sql).arg(bind.join(" and "));
    }
    if(!orderBy.isEmpty()){
        sql = QString("%1 order by %2").arg(sql).arg(orderBy.join(","));
    }
    if(!limit.isEmpty()){
        sql = QString("%1 limit %2").arg(sql).arg(limit);
    }
    sql.append(";");
    return this->list(sql,args).first();
}
