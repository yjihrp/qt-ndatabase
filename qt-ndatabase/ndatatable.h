#ifndef NDATATABLE_H
#define NDATATABLE_H

#include <QObject>
#include <QDebug>

// 查询结果
class NDataTable
{
public:
    explicit NDataTable();

    // 是否为空
    bool isEmpty();
    // 包含列
    bool hasColumn(QString column);

    // 第一行
    QVariantMap first();
    // 第 N 行
    QVariantMap at(int idx=0);
    QVariant at(int row,QString column);
    QVariant at(int row,int column);

    // 最后一行
    QVariantMap last();
    // 第一行第一列值
    QVariant scalar();
    // 数据行数
    int count();

    // 列
    QList<QString> columns;
    // 行
    QList<QVariantMap> rows;
};

#endif // NDATATABLE_H
