#ifndef NSQLITE_H
#define NSQLITE_H

#include <QObject>
#include <QDebug>

#include "ndatasession.h"

// sqlite 数据库连接
class NSqlite :public NLimitSession
{
public:
    explicit NSqlite(QString dbName,QString user="",QString password=""):NLimitSession(){
        auto connectionName = QSqlDatabase::defaultConnection;
        if(!QSqlDatabase::contains(connectionName)){
            this->connection = QSqlDatabase::addDatabase("QSQLITE",connectionName);
        }else{
            this->connection = QSqlDatabase::database(connectionName);
        }
        this->connection.setDatabaseName(dbName);
        if(!user.isEmpty()){
            this->connection.setUserName(user);
        }
        if(password.isEmpty()){
            this->connection.setPassword(password);
        }
    }
};

#endif // NSQLITE_H
