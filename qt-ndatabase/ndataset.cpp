#include "ndataset.h"

NDataSet::NDataSet(){
    this->sql = QString();
    this->mArgs = QMap<QString,QVariant>();
    this->error = QSqlError("","",QSqlError::NoError);
    this->affected = 0;
    this->lastId = QVariant();
    this->tables = QList<NDataTable>();
}

NDataSet::NDataSet(QString sql,QVariantList args,QSqlError error){
    this->sql = sql;
    for(int i=0;i<args.count();i++){
        this->mArgs[QString::number(i)] = args.at(i);
    }
    this->error = error;
    this->affected = 0;
    this->lastId = QVariant();
    this->tables = QList<NDataTable>();
}

NDataSet::NDataSet(QSqlQuery query){
    this->read(query);
}

bool NDataSet::isEmpty(){
    if(this->tables.count() == 0){
        return true;
    }
    return false;
}

bool NDataSet::hasError(){
    return this->error.type() != QSqlError::NoError;
}

QString NDataSet::message(){
    if(this->hasError()){
        return this->error.text();
    }
    return QString("");
}

void NDataSet::print(bool count,bool lastAffect){
    qDebug() << "sql:" << this->sql;
    qDebug() << "args:"<< this->mArgs;
    qDebug() << "error:" << this->hasError();
    qDebug() << "message:" << this->message();
    if(count){
        qDebug() << "table:" << this->count();
    }
    if(lastAffect){
        qDebug() << "lastId:" << this->lastId;
        qDebug() << "affected:" << this->affected;
    }
    qDebug();
}

NDataTable NDataSet::first(){
    if(this->tables.count() == 0){
        return NDataTable();
    }
    return this->tables.at(0);
}

NDataTable NDataSet::at(int idx){
    if(idx < 0){
        return NDataTable();
    }
    if(idx > this->tables.count()){
        return NDataTable();
    }
    if(this->tables.count() == 0){
        return NDataTable();
    }
    return this->tables.at(idx);
}

NDataTable NDataSet::last(){
    if(this->tables.count() == 0){
        return NDataTable();
    }
    return this->tables.last();
}

QVariant NDataSet::scalar(){
    return this->first().scalar();
}

int NDataSet::count(){
    return this->tables.count();
}

QMap<QString,QVariant> NDataSet::args(){
    return this->mArgs;
}

QVariant NDataSet::args(int index){
    if(index < 0){
        return QVariant();
    }
    auto keys = this->mArgs.keys();
    if(index >= keys.count()){
        return QVariant();
    }
    return this->mArgs[keys.at(index)];
}

QVariant NDataSet::args(QString name){
    if(name.isEmpty()){
        return QVariant();
    }
    if(!this->mArgs.contains(name)){
        return QVariant();
    }
    return this->mArgs[name];
}

void NDataSet::read(QSqlQuery query){
    this->sql = query.lastQuery();
    this->mArgs = query.boundValues();
    this->lastId = query.lastInsertId();
    this->affected = query.numRowsAffected();
    this->error = query.lastError();
    do{
        auto record = query.record();
        if(record.count() == 0){continue;}

        // get columns
        NDataTable table;
        QVector<QSqlField> columns;
        for(int i=0;i<record.count();i++){
            auto field = record.field(i);
            columns.append(field);
            table.columns.append(field.name());
        }

        // get rows
        while (query.next()){
            QVariantMap row;
            for(int i=0;i<columns.count();i++){
                auto item = columns.at(i);
                auto name = item.name();
                auto value = query.value(name);
                row[name] = value;
            }
            table.rows.append(row);
        }
        this->tables.append(table);
    }while (query.nextResult());
}
