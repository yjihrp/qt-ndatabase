#ifndef NDATASET_H
#define NDATASET_H

#include <QObject>
#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include <QSqlRecord>
#include <QSqlField>

#include "ndatatable.h"

// sql 执行结果集
class NDataSet
{
public:
    explicit NDataSet();
    NDataSet(QString sql,QVariantList args,QSqlError error);
    NDataSet(QSqlQuery query);

public:
    // 是否为空
    bool isEmpty();
    // 是否有错
    bool hasError();
    // 错误信息
    QString message();
    // 打印 sql 信息
    void print(bool count=true,bool lastAffect=false);

    // 获取绑定参数
    QMap<QString,QVariant> args();
    QVariant args(int index);
    QVariant args(QString name);

    // 第一个结果集
    NDataTable first();
    // 第 N 个结果集
    NDataTable at(int idx=0);
    // 最后一个结果集
    NDataTable last();
    // 第一个结果集的第一行第一列值
    QVariant scalar();
    // 结果集个数
    int count();

private:
    // 读取结果
    void read(QSqlQuery);

public:
    // 执行的 sql
    QString sql;
    // 错误
    QSqlError error;

    // 影响行数
    int affected;
    // 最后插入 ID
    QVariant lastId;
    // 结果集
    QList<NDataTable> tables;

private:
    // 执行后的参数值
    QMap<QString,QVariant> mArgs;
};


#endif // NDATASET_H
