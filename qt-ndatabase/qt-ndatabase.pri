QT += core sql

INCLUDEPATH += $$PWD

HEADERS += \
    $$PWD/ndatasession.h \
    $$PWD/ndataset.h \
    $$PWD/ndatatable.h \
    $$PWD/nmysql.h \
    $$PWD/nsqlite.h

SOURCES += \
    $$PWD/ndatasession.cpp \
    $$PWD/ndataset.cpp \
    $$PWD/ndatatable.cpp
