#include <QApplication>
#include <QDebug>

#include "testmysql.h"
#include "testsqlite.h"

static QString LOG_PATTERN="[%{time HH:mm:ss.zzz}] [tid %{threadid}] %{if-debug}Debug%{endif}%{if-info}Info%{endif}%{if-warning}Warning%{endif}%{if-critical}Critical%{endif}%{if-fatal}Fatal%{endif} [%{function}(){%{line}}] %{message}";

int main(int argc, char *argv[])
{
    qSetMessagePattern(LOG_PATTERN);
    QApplication a(argc, argv);

    TestSqlite sqlite;
    // TestMysql mysql;

    a.exit();
    return 0;
}
