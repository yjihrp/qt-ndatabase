#ifndef TESTMYSQL_H
#define TESTMYSQL_H

#include <QObject>

#include "nmysql.h"

class TestMysql : public QObject
{
    Q_OBJECT
public:
    explicit TestMysql(QObject *parent = nullptr):QObject(parent){
        this->args();
    }

    QVariantMap db(){
        QVariantMap conn = {
            {"user","root"},
            {"pwd","123asd!@#ASD"},
            {"host","127.0.0.1"},
            {"port",3306},
            {"database","test02"}
        };
        return conn;
    }

    void print(NDataSet data){
        qDebug() << "";
        qDebug() << "sql=" << data.sql;
        qDebug() << "args=" << data.args();
        qDebug() << "table count=" << data.count();
        qDebug() << "error=" << data.error;

        for(int i=0;i<data.count();i++){
            auto table = data.at(i);
            qDebug() << "table index" << QString::number(i);
            for(int j=0;j<table.count();j++){
                auto row = table.at(j);
                qDebug() << row;
            }
        }
    }

    void print(NDataTable data){
        qDebug() << "";
        for(int i=0;i<data.count();i++){
            auto row = data.at(i);
            qDebug() << row;
        }
    }

    void args(){
        NMysql session(this->db());
        QVariantList args = QVariantList{2};
        // 执行存储过程
        // placeholders 语法
        // 1.Oracle style colon-name(:surname)
        // 2.ODBC style (?) placeholders
        // 注:语句中不能使用 colon-name 语法
        auto data = session.list(QString("call test(?,@var1);select @var1 as v1;"),args);
        this->print(data);
    }
};

#endif // TESTMYSQL_H
