QT += core gui widgets

CONFIG += c++11

include($$PWD/../compile/compile.pri)
include($$PWD/../qt-ndatabase/qt-ndatabase.pri)

SOURCES += main.cpp

HEADERS += \
    testmysql.h \
    testsqlite.h
